#!/bin/bash


# cd app
#docker build -t http_app .
for n in $(docker ps --format "{{ .Names }}")
 do
   echo $n 
   if [ $n == "onbarding_app" ]
    then
      echo then
      docker rm -f $n
      docker run -d -p 5656:80 --name $n onboard-app
   elif [ $n == "ecom_app" ]
    then
      echo then
      docker rm -f $n
      docker run -d -p 5657:80 --name $n ecom_img
   elif [ $n == "service_app" ]
    then
      echo then
      docker rm -f $n
      docker run -d -p 5658:80 --name $n service_img
    else
      echo not available
   fi
 done


