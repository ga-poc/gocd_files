#!/bin/bash

ENV=$1

if [[ $ENV = "" ]]; then
    echo "Environment is required"
    echo "Usage: $0 {ENV}"
    exit 1
fi;

echo "=========== Testing: Start =============="
cd app
echo "inside app directory"
ls -l

sudo docker run -d -v $(pwd):/usr/src --link sonarqube newtmitch/sonar-scanner -Dsonar.projectBaseDir=my_onboarding
echo "executed docker"
sleep 5

echo "=========== Testing: Done  =============="
