#!/bin/bash

num=$1
app_name=$2
GO_PIPELINE_COUNTER=$3

cd app
sudo docker build -t $app_name .
cd ..
zip -r build-$num.zip ./app